import React, { Component } from "react";
import List from "@material-ui/core/List";
import {
  ListItem,
  ListItemText,
  Paper,
  TextField,
  Typography,
  Button
} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";

interface AppState {
  inputValue: string | undefined;
  inputData: Array<string | undefined>;
}

class App extends Component<{}, AppState> {
  constructor(props: any) {
    super(props);
    this.state = {
      inputValue: undefined,
      inputData: []
    };
  }

  handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ inputValue: event.target.value });
  };

  handleSaveBtnClick = () => {
    const { inputData, inputValue } = this.state;
    inputData.push(inputValue);
    this.setState({ inputData: inputData });
  };

  render() {
    const { inputValue, inputData } = this.state;

    return (
      <div className="App">
        <Paper style={{ padding: "1rem" }}>
          <CssBaseline />
          <div>
            <TextField
              label="Note some stuff"
              fullWidth
              margin="normal"
              variant="outlined"
              onChange={this.handleInputChange}
            />
            <Button
              fullWidth
              onClick={this.handleSaveBtnClick}
              disabled={!inputValue}
              color="primary"
            >
              Save
            </Button>
          </div>

          <List>
            {inputData.map(data => (
              <ListItem divider>
                <ListItemText>
                  <Typography variant="h6" gutterBottom>
                    {data}
                  </Typography>
                </ListItemText>
              </ListItem>
            ))}
          </List>
        </Paper>
      </div>
    );
  }
}

export default App;
